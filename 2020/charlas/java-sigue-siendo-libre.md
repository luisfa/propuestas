---
layout: 2020/post
section: propuestas
category: talks
title: Java sigue siendo libre (y gratis)
state: confirmed
---

Charla que intenta resolver la incertidumbre producida con los cambios recientes en la licencia de Java.

## Formato de la propuesta

-   [x] &nbsp;Charla (25 minutos)
-   [ ] &nbsp;Charla relámpago (10 minutos)

## Descripción

¿Has oído recientemente a alguien decirte que deberías dejar el Stack de Java porque Java va a pasar a ser de pago? ¡Yo sí! ¡Pero no es cierto! Aunque hay compañías que han cambiado su modelo de licencia de la distribución de binarios de Java, Java sigue siendo libre (y gratis).

Hace unos meses se produjeron dos anuncios sobre el modelo de releases y cambios en licencia que juntos, se han interpretado de forma errónea y ha empezado a correr el mensaje de que Java pasa a un modelo comercial.

Para clarificar este malentendido, el grupo de Java Champions, recopiló toda la información para aclarar las distintas opciones de distribución binaria y las distintas ofertas que existen.

En esta charla haremos un repaso a toda esa información, para tener claro que, aunque existen modelos comerciales, el desarrollo y la distribución de Java principal sigue siendo libre y gratis. Y hay muchas compañías comprometidas con el open source y con Java que respaldan este esfuerzo, también la comunidad.

Al final de esta charla, veremos que Java sigue teniendo un futuro brillante, adaptado, libre y gratis.

## Público objetivo

Desarrolladores o cualquier asistente que quiera conocer un poco mejor las distintas opciones de distribución de Java, tanto libres como comerciales.

Personas que hayan oído ese mensaje de "Java será de pago", "Java va a morir" y que quieran aclarar y conocer la verdadera situación de desarrollo de lenguaje y plataforma Java.

## Ponente(s)

**David Gómez G.**: Java Champion, Developer Advocate [@Liferay_es](https://twitter.com/liferay_es)

Ingeniero Técnico en Informática de Sistemas por la UPM, David lleva más de 20 años dedicado al desarrollo de software y ha participado en proyectos para Banca, Defensa, Servicios y Transporte marítimo y terrestre. Desde hace dos años, David desarrolla su labor como Developer Advocate en Liferay.

Además de como desarrollador, David realiza labores habituales de formación, ha sido Instructor Certificado para Spring Source y actualmente imparte los cursos avanzados de Java de JavaSpecialists en Español.

En los últimos 10 años, también ha estado involucrado de forma habitual y constante con varias comunidades tecnológicas de desarrollo, eventos y conferencias como ponente y organizador. Actualmente es uno de los co-organizadores del grupo de usuarios de Java en Madrid, del grupo de usuarios de Liferay y también es parte del comité técnico de Lambda World en Cádiz.

Ha sido ponente en conferencias como OpenExpo Europe 2019, Open Source Lisbon 2019, T3chFest (2014, 2016), JBCNConf (2015, 2016, 2017, 2018), Codemotion Madrid (2012, 2019), Codemotion Milan (2016), Voxxed Days Zürich (2016), OpenSouthCode (2017), Spring I/O (2011, 2012 2015), Liferay DEVCON (2018), Software Crafters Barcelona (2018). También ha dado charlas en varias comunidades locales como MadridJUG, MálagaJUG, CastellónJUG & deCharlas.com, MilanJUG, ValenciaJUG, VigoJUG, BarcelonaJUG, comunidad hispana de JUGs, MadridJS, Hackathon Lovers...

### Contacto(s)

-   **David Gómez G.**: dgomezg at gmail dot com

## Comentarios

Esta charla está basada en el documento "Java is Still Free" elaborado y mantenido por el grupo de Java Champions (<https://medium.com/@javachampions/java-is-still-free-2-0-0-6b9aa8d6d244>). Pese a la difusión que se le ha dado, recientemente aún he oido comentarios sobre la preocupación que supone que "Java pasa a un modelo comercial". Hasta el punto de que me pidieron dar esta charla en una gran organización que estaba considerando "abandonar el stack Java" por este motivo.

## Condiciones

-   [x] &nbsp;Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
-   [x] &nbsp;Al menos una persona entre los que la proponen estará presente el día programado para la charla.
