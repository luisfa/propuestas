---
layout: 2020/post
section: propuestas
category: talks
title: Seguridad ofensiva con hardware libre
state: confirmed
---

La charla pretende ser una introducción al papel del software y hardware libre en ciberseguridad ofensiva y cuáles son algunas de las herramientas fundamentales libres de un _Red Team_.

## Formato de la propuesta

-   [x] &nbsp;Charla (25 minutos)
-   [ ] &nbsp;Charla relámpago (10 minutos)

## Descripción

Una de las comunidades más fuertes de software libre es aquella que diseña, colabora y educa en ciberseguridad. Los profesionales de seguridad ofensiva (Red Team) se valen de un gran arsenal de herramientas, las cuales en su mayoría incluyen software y hardware libre. Desde el sistema operativo hasta los gadgets más específicos, la comunidad libre es el núcleo del desarrollo de un hacker ofensivo.

## Público objetivo

Va dirigida a técnicos y usuarios de Linux con curiosidad por saber cómo funciona la especialidad en seguridad.

## Ponente(s)

**Paula de la Hoz**, Senior Red Team en Telefónica Ingeniería de Seguridad.

### Contacto(s)

-   **Paula**: @terceranexus6 Telegram/Gitlab

## Comentarios

Necesito proyector y enchufe.

## Condiciones

-   [x] &nbsp;Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
-   [x] &nbsp;Al menos una persona entre los que la proponen estará presente el día programado para la charla.
