---
layout: 2020/post
section: propuestas
category: workshops
title: [Análisis de RadarCOVID: exigencias de privacidad y seguridad en App para la Administración Pública]
---

Se pretende reflexionar sobre las consecuencias de las elecciones en el desarrollo de software de cara a la libertad, y las limitaciones de índole legal: la importancia de la arquitectura de red que soporta la aplicación, o de las librerías que se elijan para el desarrollo, y las exigencias legales y las motivaciones de las mismas, cuando el código se vaya a usar por la Administración Pública. Se contata un bajo conocimiento de los requiitos técnicos por los juristas, incluso por muchos de los que se dedican a TIC, y los desarrolladores tampoco tienen muy claras las implicaciones legales, lo que conduce a fomentar una tecnología volcada totalmente en el bigdata, y en el control por unas pocas tecnológicas, que impnen su "ley" incluo contra la Ley, con merma de principios básicos para el funcionamiento de nuestra modelo de libertades públicas y, por ende, de democracia...

## Objetivos a cubrir en el taller

Los programas informáticos para Administraciones Púbicas deben estar (por ley) disponibles para éstas en código fuente, y además la norma promueve que su licencia sea libre. Valoraremos la importancia de dichos requisitos analizando si la aplicación RadarCOVID cumple las exigencias legales, y cómo un desarrollo abierto podría eliminar problemas. Se propone en modo taller porque las pruebas se harán en vivo y pueden hacerse distribuidamente, para que los distintos participantes aporten los resultados de lo que hicieran en sus propios dispositivos. trabajaremos con Android, pero quienes puedan aportar resultados con iOS serán bienvenidos. Usaremos aplicaciones de monitoreo y tracking de aplicaciones disponibles en FDroid. Este taller se podría organizar también como una charla con posteriores aportaciones de la audiencia. La finalidad es divulgar algunas normas poco conocidas, y ejemplificar la importancia de su cumplimiento. Los juritas no suelen ver las exigencias técnicas o de seguridad, mientras que los informáticos no suelen ver la exigencia de legalidad... con perjuicio para la calidad de las herramientas informáticas que usa la Administración y, por ende, del ecosistema que se fomenta.

El taller desarrollará inicialmente lo expuesto por el ponente en [¿Gran hermano o democracia?](https://ctxt.es/es/20200401/Firmas/31852/democracia-coronavirus-app-moviles-datos-personales-Luis-Fajardo-Lopez.htm), y en estas dos breves entradas, que se desarrollará: "[en entornos que requiera seguridad media o alta se exigen programas de fuentes abiertas](https://encanarias.info/posts/30135)" y "[Las redes sociales "tradicionales" no son para las Administraciones Públicas, que han de usar en su lugar redes distribuidas](https://silba.me/@lfajardo/102824588138440961)". Se pretende terminar con unas conclusiones claras, que el ponente propondrá, principalmente relativas a técnicas de desarrollo, y el problema de hacer las App dependientes del uso de infrastructuras (o librerías) de grandes corporaciones.

## Público objetivo

Cualquier ciudadano preocupado por la privacidad y el papel de lo público en el mundo de la tecnología, como correctivo de perniciosas tendencias del mercado. No son  necesarios conocimientos jurídicos ni informáticos, pero ambos son bienvenidos. De especial utiliad para informáticos que trabajen en el ámbito de la Administración, Delegados de Protección de Datos, y juristas dedicados a TIC.

## Ponente(s)

Dr. Luis Fajardo López. de perfil claramente universitario, ha sido Juez y Abogado. Fue responsable de las redes de la Facultad de Derecho de la UAM en los años 90, siendo becario de Formación de Profesorado Universitaario (FPI/FPU). Ha impartido docencia en las Universidades Autónoma de Madrid. Almería, Gerona, UNED y La Laguna, donde lo sigue haciendo en la actualidad, adscrito al Área de Derecho civil. Ha sido fundador de varias aociaciones, entre ellas el Centro de Alternativas Legales, y miembro de Madrid Wireless, el Grupo de Usuario Linux de Canarias (GULiC), Secretario de ESLIC (Empresas de Software Libre de Canarias), y la Oficina del Software Libre de la ULL, con la que viene colaborando desde hace muchos años. Desde 1999 usa exclusivamente GNU/Linux en sus sistemas. Mantiene y administra diversos nodos del Fediverso, como https://encanarias.info (Diaspora) y https://silba.me (Mastodon). Trabaja la materia desde hace tiempo, como puede verse en el [numero 181 de Novática](http://www2.ati.es/novatica/2006/181/nv181sum.html) (2006), en la que ya se mantenía, con una normativa menos específica, prácticamente las mismas consecuencias.

### Contacto(s)

* Nombre: Luis Fajardo

luisfa @ GitLab

## Prerrequisitos para los asistentes

No hay requisitos mínimos, pero se recomienda tener instalado F-Droid para facilitar la instalación de los aplicativos que serán usados. El análisis a analizar el muy somero, el mínimo necsario para divulgar cómo controlar qué usamos en nuestros dispositivos, y sacar las consecuenias jurídicas buscadas (¿cumple o no cumple la normativa?).

## Prerrequisitos para la organización

Streaming que sea conforme con protección de datos, canal del participación de los usuario. Puede ser conveniente un repositorio donde los participantes puedan compartir los resultados de sus análisis colaborativos (GitLab, o Nextcloud). Puede proporcionarlo también el ponente.

## Tiempo

Tres horas. Puede organizarse en módulos más pequeños para facilitar la asistencia. Abierto a otras participaciones o colaboraciones. Contactar con el ponente/organizador.

## Día

Preferentemente el 18.

## Comentarios

Abierto a que cualquiera sume análisis de la App propuesta, antes o durante el taller.

## Condiciones

* [x] &nbsp;Acepto seguir el [código de conducta](https://eslib.re/conducta/).
* [x] &nbsp;Al menos una persona entre los que proponen el taller estará presente el día programado para el mismo.
* [x] &nbsp;Acepto coordinarme con la organización de esLibre.
